/**
 *  Created by Colcamex Resources Inc. and Jamil Devsi for the Centre for Relationship Based Care.
 *  Intended for use through the Creative Commons General License.
 *	All rights reserved 2016.
 */

//---> BUILD MEDICATIONS, JSON BUNDLES, DOCUMENT READY FUNCTIONS, ECT <--- //

$(document).ready(function(){	
	retrieveMedications();
	restoreAllJSONBundles();
	copyCaseId();
	
	// Navigation
//	$("#section_button").click(function(){
//		$("#main_menu li").
//	})
	
	$("ul#section_menu li a").click(function(){
		var $active = $('.tab-content .tab-pane + .active');
        $active.next().removeClass('disabled');
        nextTab($active);
		
		$("a[href='#" + this.href.split("#")[1] + "']").css('color','red');
	});
	
	
	$("#case_number").focusout(function(){
		copyCaseId();
	}); 
		
});

// copy the main case id into each page heading.
function copyCaseId() {	
	var casenumber = $("#case_number").val();

	$(".case_number_top").each(function(){
		alert(this.toSource());
		//$(this).val(casenumber);
	});	
}



// ---> MAIN FORM CONTROLS <--- //

function printSubmit() {
	submission();
	printDocument();
}

function submission() {
	saveAllJSONBundles();
	setFlag();
	document.rai_hc.submit();
}

function printDocument() {
	window.print();
}

function setFlag() {
	// indicate that the form has been submitted
	if (document.getElementById("rai_hc").value == 'True') {
		document.getElementById("rai_hc").value = 'False';
	}
}

function showButtons() {
	//show the bottom buttons if they are hidden
	if (document.getElementById('BottomButtons').style.display == 'none') {
		document.getElementById('BottomButtons').style.display = '';
	}
}

//---> MEDICATION TABLES <--- //

function retrieveMedications() {
	var medications = $("#oscar_drug_list").val().split(",");
	for(var i = 0; i < medications.length; i++ ) {
		$("#" + i + "_med_name").val(medications[i]);
	}
}


//---> JSON BUNDLES <--- //

function saveAllJSONBundles() {
    $("fieldset").each(function(){
    	saveJSONBundle( this )
    })
}

function restoreAllJSONBundles() {   
	$(".jsonBundle").each(function(){
		restoreJSONBundle( this );
     })  
}

function restoreJSONBundle( element ) {
	
    // data comes back from the database as base64 encoded.
	var jsonString = atob( $(element).val() );
    var json = JSON.parse( jsonString );
    // filter 1
    var jsonBundle = json.jsonRAIHC;
    var elementId = element.name;
    var id;
    var value;
    var row = 0;
    var count = 0;
  
    // filter 2
    if( element.id == element.name + "_dataelement" ) {
	    for( var i in jsonBundle ) {   
	        id = jsonBundle[i].name;
	        value = jsonBundle[i].value;
	        // add the values to each row.
	        $( "#" + elementId + " #" + id ).val( value );
	    }
    }
}

function saveJSONBundle( element ) {

	 var data = {jsonRAIHC:[]};
     var dataelement = $( "#" + element.id + "_dataelement" ); 
     var jsonString = "";
     
    // Don't forget to add any new element tags to this selector
    // They will not get saved otherwise.
    $(element).find("input, select, textarea").each( function(){                                      
        var name = this.id;
        var value = this.value;
  
        if( name && value ) {
            data.jsonRAIHC.push({
                "name"  : name,
                "value" : value
            });
        }
    })
                                         
    // store the data into a corresponding element for the oscar db
    // base 64 encoded <-- very important
    $(dataelement).val("");
    jsonString = JSON.stringify( data );
    $(dataelement).val( btoa( jsonString ) ); 

}


// JD edits
// This is the function that is called when the generate button is clicked. It will dispatch the rest of the functions to calculate the various scales.
function doGenerate(){
	calculateADLLongFormScale();
	calculateADLSelfPerformaceHScale();
	calculateCHESSScale();
	calculateCPSScale();
	calculateDRSScale();
	calculateIADLDiffScale();
	calculateIADLInvolScale();
	calculateInterRAISPressureScale();
	calculatePainScale();
	calculateDIVERT();
	calculateMAPLe();
}

// Assigns the weighted scale to the numbers based on the tables in the document. Default case can be changed based on what we thing is good??
// Same scale is used for the Activities of Daily Living Long Form Scale and Activities of Daily Living Self-Performance Heirarchy Scale
function transformScoreADLLong(num){
	switch(num){
		case 0: return 0;
		case 1: return 0;
		case 2: return 1;	
		case 3: return 2;
		case 4: return 3;
		case 5: return 3;
		case 6: return 4;
		case 8: return 4;
		default: return -1;		
	}
}

// Calculates the ADL Long Form Scale Score from values in the eForm
function calculateADLLongFormScale(){
	// Care about 7 items
	// Mobility in Bed; Transfer; Locomotion in Home; Dressing; Eating; Toilet Use; Personal Hygiene
	var bed = transformScoreADLLong(parseInt(document.getElementById("adl_bed").value));
	var transfer = transformScoreADLLong(parseInt(document.getElementById("adl_transfer").value));
	var locomotionHome = transformScoreADLLong(parseInt(document.getElementById("adl_loco_home").value));

	//Upper or lower dressing?
	var dressUpper = transformScoreADLLong(parseInt(document.getElementById("adl_dress_upper").value));
	var dressLower = transformScoreADLLong(parseInt(document.getElementById("adl_dress_lower").value)); // not inculuded. should it be? Does not specify in the document 
	var eating = transformScoreADLLong(parseInt(document.getElementById("adl_eating").value));
	var toiletUse = transformScoreADLLong(parseInt(document.getElementById("adl_toilet").value));
	var personalHygiene = transformScoreADLLong(parseInt(document.getElementById("adl_hygiene").value));

	if (bed == -1 || transfer == -1 || locomotionHome == -1 || dressUpper == -1 || eating == -1 || toiletUse == -1 || personalHygiene == -1){
		document.getElementById("ADLLong").value = " - ";
	} else {
		document.getElementById("ADLLong").value = bed + transfer + locomotionHome +
		dressUpper + eating + toiletUse + personalHygiene;
	}
}

// Calculates ADL Self Performance Heirarchy Scale Score from values in the eForm
function calculateADLSelfPerformaceHScale(){
	var personalHygiene = transformScoreADLLong(parseInt(document.getElementById("adl_hygiene").value));
	var toiletUse = transformScoreADLLong(parseInt(document.getElementById("adl_toilet").value));
	var locomotionHome = transformScoreADLLong(parseInt(document.getElementById("adl_loco_home").value));
	var eating = transformScoreADLLong(parseInt(document.getElementById("adl_eating").value));
	var update = 0;

	if (personalHygiene == 4 && toiletUse == 4 && locomotionHome ==4 && eating == 4){
		update = 6;
	} else if (eating == 4 || locomotionHome == 4){
		update = 5;
	} else if (eating == 3 || locomotionHome == 3){
		update = 4;
	} else if (toiletUse >=3 || personalHygiene >=3){
		update = 3;
	} else if (personalHygiene ==2 || toiletUse ==2 || locomotionHome ==2 || eating ==2){
		update = 2;
	} else if (personalHygiene ==1 || toiletUse ==1 || locomotionHome ==1 || eating ==1){
		update = 1;
	} else {
		update = 0;
	}

	if (personalHygiene == -1 || toiletUse == -1 || locomotionHome ==-1 || eating ==-1 ){
		document.getElementById("ADLSPHS").value = " - ";
	} else{
		document.getElementById("ADLSPHS").value = update;
	}
}


// Changes in Health, End-Stage Disease, Signs and Symptoms Scale (CHESS) Score from values in the eForm
// 0/1 validation on the eForm so we check for isNaN to determine if appropriate input
function calculateCHESSScale(){
	// elements: 
	//B2b: Decline in cognition -> Worsening of decision making
	//H3: Decline in ADL -> ADL Decline
	//L2c: Dehydration -> Insufficient fluid
	//K2e: Vomiting -> vomiting
	//K3d: Edema -> Edema
	//K3e: SOB -> SOB
	//k8e: End-Stage Disease -> Less than 6 months to live
	//L1a: Weight Loss -> Unintended weight loss
	//L2b: Leaving food uneaten->  Noticable decrease in food/fluid 
	var declineCogniton = parseInt(document.getElementById("cog_worsening").value);
	var ADLDecline = parseInt(document.getElementById("adl_decline").value);
	var insufficientFluid = parseInt(document.getElementById("L2c").value);
	var edema = document.getElementById("K3d").checked; // checkbox
	var sob = document.getElementById("cond_breathing").checked; // checkbox
	var endStageDisease = document.getElementById("K8e").checked; //checkbox
	var weightLoss = parseInt(document.getElementById("wt_loss").value);
	var decreaseinFood = parseInt(document.getElementById("L2b").value);
	var vomiting = document.getElementById("K2e").checked; //checkbox
	var score = 0;
	var subScore = 0;

	//  3 additions
	if (declineCogniton == 1){
		score = score + 1;
	}

	if (ADLDecline ==1){
		score = score + 1;
	}

	if (endStageDisease == true){
		score = score + 1;
	}

	if (vomiting == true){
		subScore = subScore + 1;
	}

	if (edema == true){
		subScore = subScore + 1;
	}

	if (sob==true){
		subScore = subScore + 1;
	}

	if (weightLoss == 1){
		subScore = subScore + 1;
	}

	if (decreaseinFood == 1){
		subScore = subScore + 1;
	}

	if (insufficientFluid == 1){
		subScore = subScore + 1;
	}

	if (subScore >= 2){
		score = score + 2;
	} else {
		score = score + subScore;
	}

	// if (isNaN(declineCogniton) || isNaN(ADLDecline) || isNaN(insufficientFluid) || edema == false || SOB == false || endStageDisease == false || isNaN(weightloss) || isNaN(decreaseinFood) || vomiting==false){
	if (isNaN(declineCogniton) || isNaN(ADLDecline) || isNaN(insufficientFluid) || isNaN(weightLoss) || isNaN(decreaseinFood) || score > 5){
		document.getElementById("CHESS").value = " - ";
	} else {
		document.getElementById("CHESS").value = score;
	}
	// document.getElementById("CHESS").value = score;
}
// Calculates the Cognitive Performance Scale Score from values in the eForm
function calculateCPSScale(){
	// B1a: Short-Term Memory
	// B2a: Cognition Skills for Daily Decision-Making
	// C2: Expressive Communication
	// H2g: Eating
	var shortTermMemory = parseInt(document.getElementById("short_term").value);
	var cogSkillsDecisionMaking = parseInt(document.getElementById("decision_making").value);
	var expressiveCommunication = parseInt(document.getElementById("expression").value);
	var eating = parseInt(document.getElementById("adl_eating").value);

	var impairmentCount = 0;
	var severeImpairmentCount = 0;

	var score = 0;

	// calculate impairment count
	if (cogSkillsDecisionMaking == 1 || cogSkillsDecisionMaking == 2 || cogSkillsDecisionMaking == 3){
		impairmentCount = impairmentCount + 1;
	}

	if (expressiveCommunication == 1 || expressiveCommunication == 2 || expressiveCommunication == 3){
		impairmentCount = impairmentCount + 1;
	}

	if (shortTermMemory == 1){
		impairmentCount = impairmentCount + 1;
	}

	// calculate severe impairment count
	if (cogSkillsDecisionMaking == 2 || cogSkillsDecisionMaking == 3){
		severeImpairmentCount = severeImpairmentCount + 1;
	}

	if (expressiveCommunication == 3 || expressiveCommunication == 4){
		severeImpairmentCount = severeImpairmentCount + 1;
	}

	// calculate the CPS
	if (cogSkillsDecisionMaking == 0 || cogSkillsDecisionMaking == 1 || cogSkillsDecisionMaking == 2 || cogSkillsDecisionMaking == 3){
		if (impairmentCount == 0){
			score = 0;
		} else if (impairmentCount == 1){
			score = 1;
		} else if (impairmentCount == 2 || impairmentCount == 3){
			if (severeImpairmentCount == 0){
				score = 2;
			} else if (severeImpairmentCount == 1){
				score = 3;
			} else if (severeImpairmentCount == 2){
				score = 4;
			}
		}
	} else if (cogSkillsDecisionMaking == 4){
		if (eating == 0 || eating == 1 || eating == 2 || eating == 3 || eating == 4 || eating == 5){
			score = 5;
		} else if (eating == 6 || eating == 8){
			score = 6;
		}
	}


	if(isNaN(shortTermMemory) || isNaN(cogSkillsDecisionMaking) || isNaN(expressiveCommunication) || isNaN(eating) || expressiveCommunication > 4){
		document.getElementById("CPS").value = " - ";
	} else{
		document.getElementById("CPS").value = score;
	}
}

// Calculates the Depression Rating Scale (DRS) from values in the eForm
function calculateDRSScale(){
	// E1a: feeling of sadness or being depressed
	// E1b: persistent anger with self/others
	// E1c: Expression of unrealistic fears
	// E1d: Repetitive health complaints
	// E1e: Repetitive anxious complaints
	// E1f: Sad, pained, worried facial expressions
	// E1g: Recurrent crying, tearfulness
	var feelSad = parseInt(document.getElementById("mood_sadness").value);
	var persistentAnger = parseInt(document.getElementById("mood_anger").value);
	var unrealisticFears = parseInt(document.getElementById("mood_fears").value);
	var healthComplaints = parseInt(document.getElementById("mood_complaints").value);
	var anxiousComplaints = parseInt(document.getElementById("mood_anxious").value);
	var facialExpressions = parseInt(document.getElementById("mood_facial_expr").value);
	var recurrentCrying = parseInt(document.getElementById("mood_crying").value);

	var score = feelSad + persistentAnger + unrealisticFears + healthComplaints + anxiousComplaints + facialExpressions + recurrentCrying;

	if (isNaN(feelSad) || isNaN(persistentAnger) || isNaN(unrealisticFears) || isNaN(healthComplaints) || isNaN(anxiousComplaints) || isNaN(facialExpressions) || isNaN(recurrentCrying) || score>14){
		document.getElementById("DRS").value = " - ";
	} else {
		document.getElementById("DRS").value = score; 
	}
}

// Calculates the IADL Difficulty Scale from values in the eForm
function calculateIADLDiffScale(){
	// H1aB: Meal preparation
	// H1bB: Ordinary housework
	// H1eB: Phone use
	var mealPrep = parseInt(document.getElementById("dif_iadl_meal").value);
	var housework = parseInt(document.getElementById("dif_iadl_housework").value);
	var phoneUse = parseInt(document.getElementById("dif_iadl_phone").value);
	var score = 0;

	score = mealPrep + housework + phoneUse;

	if (isNaN(mealPrep) || isNaN(housework) || isNaN(phoneUse) || score >6){
		document.getElementById("IADL").value = " - ";
	} else {
		document.getElementById("IADL").value = score;
	}
}


// Helper function that does the check and adjusment for weighted scale for calculateIADLInvolScale
function transformScoreIADLInvolScale(num){
	if (num == 8){
		return 3;   // converted to "By others with a score of 3"
	} else {
		return num;
	}
}
// Calculates the IADL Involvement Scale from values in the eForm
function calculateIADLInvolScale(){
	// H1aA: Meal Prep
	// H1bA: Ordinary housework
	// H1cA: Managing finances
	// H1dA: Managing medications
	// H1eA: Phone use
	// H1fA: Shopping
	// H1gA: Transportation
	var mealPrep = transformScoreIADLInvolScale(parseInt(document.getElementById("per_iadl_meal").value));
	var housework = transformScoreIADLInvolScale(parseInt(document.getElementById("per_iadl_housework").value));
	var finances = transformScoreIADLInvolScale(parseInt(document.getElementById("per_iadl_finances").value));
	var meds = transformScoreIADLInvolScale(parseInt(document.getElementById("per_iadl_medications").value));
	var phone = transformScoreIADLInvolScale(parseInt(document.getElementById("per_iadl_phone").value));
	var shopping = transformScoreIADLInvolScale(parseInt(document.getElementById("per_iadl_shopping").value));
	var transportation = transformScoreIADLInvolScale(parseInt(document.getElementById("per_iadl_transport").value));
	var score = 0;

	score = mealPrep + housework + finances + meds + phone + shopping + transportation;

	if (isNaN(mealPrep) || isNaN(housework) || isNaN(finances) || isNaN(meds) || isNaN(phone) || isNaN(shopping) || isNaN(transportation) || score > 21){
		document.getElementById("IADLIn").value = " - ";
	} else {
		document.getElementById("IADLIn").value = score;
	}
}

// Calculates the interRAI Pressure Ulcer Risk Scale
function calculateInterRAISPressureScale(){
	// H2a Mobility in Bed
	// H2c Locomotion in Home
	// I3 Bowel Continence
	// K3e Shortness of breath // checkbox
	// K4a Pain Frequency
	// L1a Unintended weight loss
	// N4 Prior Pressure Ulcer
	var mobilityInBed = parseInt(document.getElementById("adl_bed").value);
	var locomotionInHome = parseInt(document.getElementById("adl_loco_home").value);
	var bowelContinence = parseInt(document.getElementById("bowel_continence").value);
	var sob = document.getElementById("cond_breathing").checked; // checkbox 
	var painFrequency = parseInt(document.getElementById("pain_frequency").value);
	var unintendedWeightLoss =parseInt(document.getElementById("wt_loss").value); 
	var priorPressureUlcer = parseInt(document.getElementById("prior_ulcer").value); 
	var score = 0;

	if (mobilityInBed == 4 || mobilityInBed == 5 || mobilityInBed == 6 || mobilityInBed == 8){
		score = score + 1;
	}

	if (locomotionInHome == 4 || locomotionInHome == 5 || locomotionInHome == 6 || locomotionInHome == 8){
		score = score + 1;
	}

	if (bowelContinence == 4 || bowelContinence == 5 || bowelContinence == 6 || bowelContinence == 8){
		score = score + 1;
	}

	if (sob == true){
		score = score + 1;
	}

	if (painFrequency == 2 || painFrequency == 3){
		score = score + 1;
	}

	if (unintendedWeightLoss == 1){
		score = score + 1; 
	}

	if (priorPressureUlcer == 1){
		score = score + 2;
	}

	if (isNaN(mobilityInBed) || isNaN(locomotionInHome) || isNaN(bowelContinence) || isNaN(painFrequency) || isNaN(unintendedWeightLoss) || isNaN(priorPressureUlcer)){
		document.getElementById("IRAIS").value = " - ";
	} else {
		document.getElementById("IRAIS").value = score;
	}
}

// calculates the Pain Scale from elements in the webform
function calculatePainScale(){
	// K4a: Pain Frequency
	// K4b: Pain Intensity
	var painFrequency = parseInt(document.getElementById("pain_frequency").value);
	var painIntensity = parseInt(document.getElementById("pain_intensity").value);
	var score = 0;

	if (painFrequency == 2 || painFrequency == 3){
		if (painIntensity == 1 || painIntensity == 2){
			score = 2;
		} else if (painIntensity == 3 || painIntensity == 4){
			score = 3;
		}
	}  else if (painFrequency == 0){
		score = 0;
	} else if (painFrequency == 1){
		score = 1;
	}

	if (isNaN(painFrequency) || isNaN(painIntensity)){
		document.getElementById("PainScale").value = " - ";
	} else {
		document.getElementById("PainScale").value = score;
	}
}

// calculate Cardio-Respiratory CAP - determines if the CAP is triggered
// Triggered if one of: Chest Pain; SOB; Irregular pulse; Dizziness; 
// * Also any of the following that are not present on the interRAI assessment forms: Systolic blood pressure >200 or < 100; RR >20; HR >100 or <50 per min; Oxygenation sat <94%
function calculateCardioRespCAP(){
	// added based on document provided by Dennis: interRAI's Clincal Assessment Protocols Manual Page 126-127 "Cardio-Respiratory Conditions Clinical Assessment Protocol"
	// K3a = chest pain
	// K3c = dizziness
	// K3e = shortness of breath
	// J1e = irregular pulse
	// FOR later development add in the extra elements that are not captured on the RAI form see * above
	var chestPain = document.getElementById("cond_chest_pain").checked;
	var dizziness = document.getElementById("cond_dizziness").checked;
	var sob = document.getElementById("cond_breathing").checked;
	var irregularPulse = parseInt(document.getElementById("J1e").value); 

	if (chestPain == true || dizziness == true || sob == true || irregularPulse == 1){
		return true;
	} else {
		return false;
	}
}

// calculates the Detection of Indicators and Vulnerabilities for Emergency Room Trips Screening Algorithms (DIVERT)
function calculateDIVERT(){
	// H3: ADL Decline valvues 0/1
	// H7c: Good prospects of recovery from current disease or conditions, improved health status expected // checkbox
	// I2b: Indwelling Catheter //checkbox
	// J1a: Stroke 0/1
	// J1b: CHF 0/1
	// J1c: Coronary heart disease 0/1
	// J1u: Pneumonia 0/1
	// J1w: UTI 0/1
	// J1y: DM 0/1
	// J1z: Asthma 0/1
	// J1aa: Renal failure 0/1
	// K5 Falls Frequency -> fall_frequency 0-9
	// L1a: unintended weight loss 0/1
	// L2b: decrease in food or fluids 0/1
	// N2b: Stasis ulcer 0-4
	// P2a: Oxygen   -> if none of the above checked -> false or blank or  to 3
	// P4a: hospital 0-9
	// P4b: ED use 0-9
	// Depression Rating Scale (0-14)
	// Cardio-Respiratory Conditions CAP
	var adlDecline = parseInt(document.getElementById("adl_decline").value);
	var prospectsRecovery = document.getElementById("pot_recovery").checked; // checkbox
	var indwellingCatheter =  document.getElementById("bladder_catheter").checked; // checkbox
	var stroke =  parseInt(document.getElementById("J1a").value);
	var chf = parseInt(document.getElementById("J1b").value);
	var coronaryHeartDisease = parseInt(document.getElementById("J1c").value);
	var pneumonia = parseInt(document.getElementById("J1u").value);
	var uti = parseInt(document.getElementById("J1w").value);
	var diabetes = parseInt(document.getElementById("J1y").value);
	var asthmaOrCOPD = parseInt(document.getElementById("J1z").value);
	var renalFailure = parseInt(document.getElementById("J1aa").value); // not used in algorithm? not included in calucation condition at the end
	var fallsFrequency = parseInt(document.getElementById("fall_frequency").value);
	var unintendedWeightLoss = parseInt(document.getElementById("wt_loss").value);
	var decreaseinFoodFluids = parseInt(document.getElementById("wt_cachexia").value); 
	var stasisUlcer = parseInt(document.getElementById("stasis_ulcer").value); 
	var oxygen = parseInt(document.getElementById("sttp_resp_oxygen").value); 
	var hospital = parseInt(document.getElementById("visit_hospital").value); 
	var emergency = parseInt(document.getElementById("visit_emergency").value); 
	var depression = document.getElementById("DRS").value;
	var cardioRespCAPTriggered = calculateCardioRespCAP();
	var irregularPulse = parseInt(document.getElementById("J1e").value); // to check if isNaN for Cap
	var score = 0;

	if (emergency >=2 || hospital >=2){
		if (cardioRespCAPTriggered == true){
			score = 6;
		} else {
			if (indwellingCatheter == true || uti == 1){
				score = 6;
			} else {
				score = 5;
			}
		}
	} else if (emergency ==1 || hospital ==1){
		if (cardioRespCAPTriggered == true){
			if (chf == 1 || coronaryHeartDisease == 1){
				score = 5;
			} else {
				score = 4;
			}
		} else {
			if (prospectsRecovery == true){
				if (depression >=1){
					score = 4;
				} else {
					score = 3;
				}
			} else {
				score = 2;
			}
		}
	} else if (emergency ==0 || hospital ==0){
		if (cardioRespCAPTriggered ==  true){
			if (chf == 1 || coronaryHeartDisease == 1){
				if (pneumonia == 1 || uti == 1 || asthmaOrCOPD == 1){
					score = 4;
				} else {
					score = 3;
				} 
			} else {
				if (unintendedWeightLoss == 1 || decreaseinFoodFluids == 1){
					score = 3;
				} else {
					// blank is a valid input on the form so if not blank we are assuming that the oxygen value should be true even if they havent recieved it yet? See section P2 on RAI HC form
					if (!isNaN(oxygen)){
						score = 4;
					} else {
						score = 2;
					}
				}
			}
		} else {
			// any falls -> >=1 fall
			if (fallsFrequency >=1){
				if (diabetes == 1 || stroke == 1){
					score = 3;
				} else {
					score = 2;
				}
			} else {
				// any grade of ulcer will be counted as ulcer
				if (stasisUlcer >= 1){
					score = 2;
				} else {
					if (adlDecline == 1){
						score = 2;
					} else {
						if (unintendedWeightLoss == 1 || decreaseinFoodFluids == 1){
							score = 2;
						} else {
							score = 1;
						}
					}
				}
			}
		}
	}
	
	if (isNaN(adlDecline) || isNaN(stroke) || isNaN(chf) || isNaN(coronaryHeartDisease) || isNaN(pneumonia) || isNaN(uti) || isNaN(diabetes) || isNaN(asthmaOrCOPD) || isNaN(fallsFrequency) || fallsFrequency >9 || isNaN(unintendedWeightLoss) || isNaN(decreaseinFoodFluids) || isNaN(stasisUlcer) || stasisUlcer >4 || oxygen > 3 || isNaN(hospital) || hospital > 9 || isNaN(emergency) || emergency > 9 || isNaN(irregularPulse)){
		document.getElementById("DIVERT").value = " - ";
	} else {
		document.getElementById("DIVERT").value = score;
	}
}

// Geriatric Screener/ self-reliance calculator
function calculateGeriatricScreen(){
	// Geriatric Screener
	var decisionMaking = parseInt(document.getElementById("decision_making").value);
	var mealPrep = parseInt(document.getElementById("dif_iadl_meal").value);
	var housework = parseInt(document.getElementById("dif_iadl_housework").value);
	var transport = parseInt(document.getElementById("dif_iadl_transport").value);
	var hygiene = parseInt(document.getElementById("adl_hygiene").value);
	var bathing = parseInt(document.getElementById("adl_bathing").value);
	var physicalActivity = parseInt(document.getElementById("stam_physical").value);
	var score = 0;

	if (isNaN(decisionMaking) || isNaN(mealPrep) || isNaN(housework) || isNaN(transport) || isNaN(hygiene) || isNaN(bathing) || isNaN(physicalActivity)){
		return "*";
	}

	// self reliance:
	if (decisionMaking >=1){
		return true;
	}

	if (mealPrep >=1){
		score = score + 1;
	}

	if (housework >=1){
		score = score + 1;
	}

	if (housework >=1){
		score = score + 1;
	}

	if (transport>=1){
		score = score + 1;
	}

	if (hygiene >=1){
		score = score + 1;
	}

	if (bathing >=1){// E3
	var wandering = parseInt(document.getElementById("behave_wandering").value);
	var verbalAbusive = parseInt(document.getElementById("behave_verb_abuse").value);
	var physicalAbusive = parseInt(document.getElementById("behave_phys_abuse").value);
	var socialDisruption = parseInt(document.getElementById("behave_disruptive").value);
	var resistCare = parseInt(document.getElementById("behave_resists").value);
		score = score + 1;
	}

	if (physicalActivity == 1){
		score = score + 1;
	}

	if (score >=4){
		return true;
	} else {
		return false;
	}

}

// Calculates the Institutional Risk Cap. Is it >=3 or >3??
function calculateInstitionalRiskCAP(){
	// E3a-e
	// CC7 - residential care facility          box
	// B1a - short-term memory                  box 
	// B2a - decision making                    box 
	// C2 - making self understood              box 
	// C3 - ability to understand others        box 
	// H2b - ADL transfer                       box 
	// H2c - ADL locomotion at home             box 
	// H2i - ADL personal hygiene               box 
	// H3 - ADL decline                         box 
	// H4a - mode of locomotion indoor          box 
	// H6a - Stamina - days outside house       box 
	// I1a bladder continence                   box 
	// J1g Alzheimers                           checkbox 
	// K5 falls frequency                       box
	
	// dont need to check if isNaN here because is checked by function caller
	var wandering = parseInt(document.getElementById("behave_wandering").value);
	var verbalAbusive = parseInt(document.getElementById("behave_verb_abuse").value);
	var physicalAbusive = parseInt(document.getElementById("behave_phys_abuse").value);
	var socialDisruption = parseInt(document.getElementById("behave_disruptive").value);
	var resistCare = parseInt(document.getElementById("behave_resists").value);
	var behaviourScore = 0;
	////////
	var residentialCare = parseInt(document.getElementById("prior_facility").value); 
	var shortTermMemory = parseInt(document.getElementById("short_term").value); 
	var decisionMaking = parseInt(document.getElementById("decision_making").value); 
	var selfUnderstood = parseInt(document.getElementById("expression").value); 
	var understandOthers = parseInt(document.getElementById("comprehension").value); 
	var transfer = parseInt(document.getElementById("adl_transfer").value); 
	var locomotionHome = parseInt(document.getElementById("adl_loco_home").value); 
	var hygiene = parseInt(document.getElementById("adl_hygiene").value); 
	var adlDecline = parseInt(document.getElementById("adl_decline").value); 
	var indoor = parseInt(document.getElementById("pri_loco_indoors").value); 
	var stamina = parseInt(document.getElementById("stam_outside").value); 
	var bladderContinence = parseInt(document.getElementById("bladder_continence").value); 
	var alzheimers = document.getElementById("J1g").checked; //checkbox
	var falls = parseInt(document.getElementById("fall_frequency").value);
	var score = 0;

	if (isNaN(residentialCare) ||isNaN(shortTermMemory) || isNaN(decisionMaking) || isNaN(selfUnderstood) || isNaN(understandOthers) || isNaN(transfer) || isNaN(locomotionHome) || isNaN(hygiene) || isNaN(adlDecline) || isNaN(indoor) || isNaN(stamina) || isNaN(bladderContinence) || isNaN(falls)){
		return "*";
	}

	// calcualtes the behavior score
	if (wandering>=1){
		behaviourScore = behaviourScore + 1;
	}
	if (verbalAbusive>=1){
		behaviourScore = behaviourScore + 1;
	}
	if (physicalAbusive>=1){
		behaviourScore = behaviourScore + 1;
	}
	if (socialDisruption>=1){
		behaviourScore = behaviourScore + 1;
	}
	if (resistCare>=1){
		behaviourScore = behaviourScore + 1;
	}
	// --- calculate score --- 

	if (behaviourScore >0){
		score = score + 1;
	}
	if (residentialCare == 1){
		score = score + 1;
	}
	if (shortTermMemory == 1){
		score = score + 1;
	}
	if (decisionMaking >= 1){
		score = score + 1;
	}
	if (selfUnderstood >= 1){
		score = score + 1;
	}
	if (understandOthers >= 1){
		score = score + 1;
	}
	if (transfer >= 3){
		score = score + 1;
	}
	if (locomotionHome >= 3){
		score = score + 1;
	}
	if (hygiene >= 3){
		score = score + 1;
	}
	if (adlDecline == 1){
		score = score + 1;
	}
	if (indoor == 4){
		score = score + 1;
	}
	if (stamina == 3){
		score = score + 1;
	}
	if (bladderContinence >= 3){
		score = score + 1;
	}
	if (alzheimers){
		score = score + 1;
	}
	if (falls>0){
		score = score + 1;
	}

	if (score >3){
		return true;
	} else {
		return false;
	}
}

// calculates the Method for Assigning Priority Levels decision-support tool from values in eForm
function calculateMAPLe(){
	// B2b: wosening of decision-making
	// E3: behaviour -> need to have all of E3
	// H1aB: meal prep
	// H1dB: med management
	// K5: falls
	// L2a: In at least 2 out of last three days, ate one or fewer meals a day
	// L3: swallowing
	// N2: pressure or stasis ulcers -> N2a or N2b?
	// O1: environment -> which one? There is a-i
	// Q1: number meds
	// ADL Self-Performance Heirarchy Scale (ADLSPHS) 0-6
	// CPS (CPS)
	// Institutional risk CAP??
	// Geriatric Screener:
	//  - B2a: decisions about organizing the day
	//  - H1aB: meal prep
	//  - H1bB: housework
	//	- H1gB: transport
	//  - H2i: hygiene
	//	- H2j: bathing
	//	- H6b: hours of physical activities
	var cogWorsening = parseInt(document.getElementById("cog_worsening").value);

	var mealPrep = parseInt(document.getElementById("dif_iadl_meal").value);
	var medManagement = parseInt(document.getElementById("dif_iadl_medications").value);
	var falls = parseInt(document.getElementById("fall_frequency").value);
	var fewMeals = parseInt(document.getElementById("cons_meals").value);
	var swallowing = parseInt(document.getElementById("swallowing").value);
	var pressureUlcer = parseInt(document.getElementById("pressure_ulcer").value);
	var stasisUlcer = parseInt(document.getElementById("stasis_ulcer").value);
	// [O1]- all checkboxes
	var lighting = document.getElementById("home_lighting").checked;
	var flooring = document.getElementById("home_flooring").checked;
	var bathroom = document.getElementById("home_bathroom").checked;
	var kitchen = document.getElementById("home_kitchen").checked;
	var heating = document.getElementById("home_heating").checked;
	var safety = document.getElementById("home_safety").checked;
	var access = document.getElementById("home_access").checked;
	var rooms = document.getElementById("home_roooms").checked;
	var none = document.getElementById("home_none").checked; // it would be the opposite of this condition/checkbox

	var numberMeds = parseInt(document.getElementById("medication_total").value);
	var ADLHScaleScore = document.getElementById("ADLSPHS").value;
	var CPSScaleScore = document.getElementById("CPS").value;
	var geriatricScreener = calculateGeriatricScreen(); // self reliance if triggered then not self reliant
	var institutionalRiskCap = calculateInstitionalRiskCAP();

	// [E3]
	var wandering = parseInt(document.getElementById("behave_wandering").value);
	var verbalAbusive = parseInt(document.getElementById("behave_verb_abuse").value);
	var physicalAbusive = parseInt(document.getElementById("behave_phys_abuse").value);
	var socialDisruption = parseInt(document.getElementById("behave_disruptive").value);
	var resistCare = parseInt(document.getElementById("behave_resists").value);

	var score = 0;

	if (ADLHScaleScore >=1){
		if (wandering>=1 || verbalAbusive>=1 || physicalAbusive>=1 || socialDisruption>=1 || resistCare>=1){
			score = 5;
		} else {
			if (CPSScaleScore >=3){
				if(falls >=2){
					score = 5;
				} else {
					score = 4;
				}
			} else {
				// I am assuming this node is OR not AND based on the other nodes
				if (fewMeals ==1 || swallowing>=1 || falls >=2){
					score = 4;
				} else {
					score = 3;
				}
			}
		}
	} else {
		if (CPSScaleScore >=2){
			if (wandering>=1 || verbalAbusive>=1 || physicalAbusive>=1 || socialDisruption>=1 || resistCare>=1){
				score =5;
			} else {
				if (wandering >=1 || institutionalRiskCap){
					score = 5;
				} else {
					score = 4;
				}
			}
		} else {
			if (wandering>=1 || verbalAbusive>=1 || physicalAbusive>=1 || socialDisruption>=1 || resistCare>=1){
				score = 4;
			} else {
				if (cogWorsening == 1){
					score = 4;
				} else {
					if (lighting || flooring || bathroom || kitchen || heating || safety || access || rooms){ // should !none be included here?
						score = 3;
					} else {
						if (pressureUlcer >= 3|| stasisUlcer >=3){
							score = 4;
						} else {
							if (geriatricScreener){
								score = 1;
							} else {
								if (mealPrep == 1){
									score = 3;
								} else {
									score = 2;
								}
							}
						}
					}
				}
			}
		}
	}
	if (geriatricScreener == "*" || institutionalRiskCap == "*" || isNaN(cogWorsening) || isNaN(mealPrep) || isNaN(medManagement) || isNaN(falls) || isNaN(fewMeals) || isNaN(swallowing) || isNaN(pressureUlcer) || isNaN(stasisUlcer) || isNaN(numberMeds) || isNaN(wandering) || isNaN(verbalAbusive) || isNaN(physicalAbusive) || isNaN(socialDisruption) || isNaN(resistCare) || ADLHScaleScore == "Error: Missing/invalid input" || CPSScaleScore == "Error: Missing/invalid input"){
		document.getElementById("MAPLe").value = " - ";
	} else {
		document.getElementById("MAPLe").value = score;
	}
}
// Questions for Dennis:
// - form validation: on the eForms the elements have a min/max for the incrementor/decrementor, however, a user can still input a value that is out of this range if typed directly into the box. Do we need to protect from values outside these ranges (ie do we need to get the algorithm to first check if a value is in range before it can use it? - it may be redandant, there are some checks and balances for the scores to prevent incorrect computation)








